#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <png.h>


// --------------------------------------------------------------------------------
// STOPWATCH FUNCTIONALLITY
// --------------------------------------------------------------------------------
double tiempo(){
	struct timespec tsp;

	clock_gettime(CLOCK_REALTIME, &tsp);

	double secs = (double)tsp.tv_sec;
	double nano = (double)tsp.tv_nsec / 1000000000.0;

	return secs + nano;
}
// --------------------------------------------------------------------------------


// --------------------------------------------------------------------------------
// FILE IO HANDLING
// --------------------------------------------------------------------------------
/*
	Retorna los pixeles de la imagen, y los datos relacionados en los
	argumentos: ancho, alto, tipo de color (normalmente RGBA) y bits por
	pixel (usualemente 8 bits) 
*/
png_bytep* abrir_archivo_png(char *filename, int *width, int *height, png_byte *color_type, png_byte *bit_depth) {
	FILE *fp = fopen(filename, "rb");

	png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) abort();

	png_infop info = png_create_info_struct(png);
	if (!info) abort();

	if (setjmp(png_jmpbuf(png))) abort();

	// Starting PNG IO segment. ------------------------------------
	png_init_io(png, fp);

	png_read_info(png, info);

	// Resolucion y color de la imagen.
	// Usaremos espacio de color RGBA
	*width      = png_get_image_width(png, info);
	*height     = png_get_image_height(png, info);
	*color_type = png_get_color_type(png, info);
	*bit_depth  = png_get_bit_depth(png, info);

	// Read any color_type into 8bit depth, RGBA format.
	// See http://www.libpng.org/pub/png/libpng-manual.txt

	if (*bit_depth == 16)
		png_set_strip_16(png);

	if (*color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_palette_to_rgb(png);

	// PNG_COLOR_TYPE_GRAY_ALPHA is always 8 or 16bit depth.
	if (*color_type == PNG_COLOR_TYPE_GRAY && *bit_depth < 8)
		png_set_expand_gray_1_2_4_to_8(png);

	if(png_get_valid(png, info, PNG_INFO_tRNS))
		png_set_tRNS_to_alpha(png);

	// These color_type don't have an alpha channel then fill it with 0xff.
	if( *color_type == PNG_COLOR_TYPE_RGB  || 
       	    *color_type == PNG_COLOR_TYPE_GRAY ||
	    *color_type == PNG_COLOR_TYPE_PALETTE )
		png_set_filler(png, 0xFF, PNG_FILLER_AFTER);

	if (*color_type == PNG_COLOR_TYPE_GRAY || 
	    *color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
		png_set_gray_to_rgb(png);

	png_read_update_info(png, info);

	png_bytep* row_pointers = (png_bytep*) malloc(sizeof(png_bytep*) * (*height));

	if (row_pointers == NULL){
		fprintf(stderr, "Error al obtener memoria de la imagen\n");
		exit(-1);
	}

	for (int y = 0; y < *height; y++) {
		row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png, info));
	}

	png_read_image(png, row_pointers);

	fclose(fp);
	// Ending PNG IO segment. --------------------------------------
	
	return row_pointers;
}

/*
	Usaremos bit depth 8 Color type PNG_COLOR_TYPE_GRAY_ALPHA
*/
void guardar_imagen_png(char *filename, int width, int height, png_byte color_type, png_byte bit_depth, png_bytep *res) {

	FILE *fp = fopen(filename, "wb");
	if (!fp) abort();

	png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png) abort();

	png_infop info = png_create_info_struct(png);
	if (!info) abort();

	if (setjmp(png_jmpbuf(png))) abort();

	// Starting PNG IO segment. -------------------------------------
	png_init_io(png, fp);

	// Salida es escala de grises
	png_set_IHDR(
		png,
		info,
		width, height,
		bit_depth,
		color_type,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT
	);

	png_write_info(png, info);

	// Writing pixel values to png image.
	png_write_image(png, res);	

	png_write_end(png, NULL);
	// Ending PNG IO segment. --------------------------------------

	// Closing file descriptor.
	fclose(fp);
}
// --------------------------------------------------------------------------------



// --------------------------------------------------------------------------------
// PNG PROCESSING FUNCTIONS
// --------------------------------------------------------------------------------
typedef struct RowData_t {
	int width;
	int height;
	int height_offset;
	int n_threads;
	png_bytep* colored_row_ptrs;
	png_bytep* gray_row_ptrs;
} RowData;

void* procesar_filas(void* raw_data){
	RowData* row_data = (RowData*) raw_data;	

	fprintf(stderr, "Hilo %d ha empezado.\n", row_data->height_offset);
	for (int y = row_data->height_offset; y < row_data->height; y += row_data->n_threads ) {

		// Espacio para la los pixeles de dicha fila
		// greyscale con alpha, 2 bytes por pixel.
		row_data->gray_row_ptrs[y] = malloc(sizeof(png_bytep) * row_data->width * 2);			

		if(row_data->gray_row_ptrs[y] == NULL){
			fprintf(stderr, "No se pudo reservar espacio para la imagen procesada");
			exit(-1);
		}

		png_bytep row = row_data->colored_row_ptrs[y];
		for (int x = 0; x < row_data->width; x++) {
			png_bytep px = &(row[x * 4]);

			// Convertimos a escala de grises
			float g  = .299f * px[0] + .587f * px[1] + .114f * px[2];
			row_data->gray_row_ptrs[y][2*x] = g;
			
			// Transparencia... dejamos el campo de transparencia igual.
			row_data->gray_row_ptrs[y][2*x + 1] = px[3]; 
		}
	}

	// The thread data was heap allocated, need to deallocate.
	free(raw_data);

	return NULL;
}

//Retorna el arreglo de pixeles
png_bytep* procesar_archivo_png(int width, int height, png_bytep* colored_row_ptrs, int n_threads) {

	//filas de nueva imagen
	png_bytep* gray_row_ptrs = (png_bytep*) malloc(sizeof(png_bytep*) * height);

	if (gray_row_ptrs == NULL){
		fprintf(stderr, "No se pudo reservar espacio para la imagen procesada");
		exit(-1);
	}

	// Create memory area to hold thread pointers.
	pthread_t* thread_ptrs = (pthread_t*) malloc(sizeof(thread_ptrs) * n_threads);

	// Create each thread to process a specific set of rows.
	for (int t = 0; t < n_threads; t++){

		// Thread will be responsible for deleting row_data.
		RowData* row_data          = (RowData*) malloc(sizeof(RowData));
		row_data->width            = width;
		row_data->height           = height;
		row_data->height_offset    = t;
		row_data->n_threads        = n_threads;
		row_data->gray_row_ptrs    = gray_row_ptrs;
		row_data->colored_row_ptrs = colored_row_ptrs;

		int err = pthread_create(thread_ptrs + t, NULL, procesar_filas, (void*) row_data);

		if (err != 0){
			fprintf(stderr, "Error durante la creacion del hilo.");
			exit(EXIT_FAILURE);
		}
	}

	// Wait for every thread to finish.
	for (int t = 0; t < n_threads; t++){

		void* return_holders = NULL;
		int err              = pthread_join(thread_ptrs[t], &return_holders);

		if (err != 0){
			fprintf(stderr, "Error esperando a union con hilo hijo.\n");
			exit(EXIT_FAILURE);
		}

		fprintf(stderr, "Hilo %d ha terminado.\n", t);
	}

	// Deallocate thread memory areas.
	free(thread_ptrs);

	return gray_row_ptrs;
}
// --------------------------------------------------------------------------------



// --------------------------------------------------------------------------------
// MAIN-THREAD OF EXECUTION BEGINS 
// --------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	// Parsing de los argumentos de linea de comando.
	if (argc < 5){
		fprintf(stderr, 
				"\nNumero de argumentos incorrecto.\n"
				"\tUSO: ./procesador_png <fuente> <destino> -n <# Hilos>\n\n");
		exit(EXIT_FAILURE);
	}

	int N = atoi(argv[4]);
	
	if (N < 0){
		fprintf(stderr, "\nArgumento -n debe ser mayor a 0\n");
		exit(EXIT_FAILURE);
	}

	if (N > 16){
		fprintf(stderr, "\nArgumento -n tiene que ser igual o menor a 16.\n");
		exit(EXIT_FAILURE);
	}

	//Datos de la imagen original
	int width, height;
	png_byte color_type; 
	png_byte bit_depth;
	png_bytep *pixeles;
	png_bytep *pixeles_res;

	// Open the file.
	fprintf(stderr, "Abriendo archivo PNG [%s].\n", argv[1]);
	pixeles = abrir_archivo_png(argv[1], &width, &height, &color_type, &bit_depth);

	// Processes the image to make it grayscale, while 
	fprintf(stderr, "Ejecutando procesamiento de imagen.\n");
	double ini = tiempo();
		pixeles_res = procesar_archivo_png(width, height, pixeles, N);
	double fin = tiempo();

	double delta = fin - ini;

        fprintf(stdout,	"%f\n", delta);
	fflush(stdout);
	
	// Save the image on disc.	
	fprintf(stderr, "Guardando imagen de resultado en disco.\n");
	guardar_imagen_png(argv[2], width, height, PNG_COLOR_TYPE_GRAY_ALPHA, bit_depth, pixeles_res);

	// De-allocating heap memory to hold pixel values.
	for(int y = 0; y < height; y++) {
		free(pixeles[y]);
		free(pixeles_res[y]);
	}

	free(pixeles);
	free(pixeles_res);


  return 0;
}
// --------------------------------------------------------------------------------
