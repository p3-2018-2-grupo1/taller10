#! /usr/bin/env perl

use v5.22;
use strict;
use utf8;

use warnings;
no warnings 'experimental::smartmatch';

use open qw( :encoding(UTF-8) :std );

=head1 sample.pl 
	Este humilde programa correrá múltiples pruebas sobre el
	programa `procesador`. Empezaremos por correr `procesador`
	con un hilo, y se irá incrementando el número de hilos 
	hasta llegar al máximo permitido de 16. Por cada hilo, se
	correrán N veces el programa `procesador` para diluir la
	influencia de datos abherrantes sobre los resultados.
=cut

# PARSE OPTIONS -----------------------------------------------------------------
use Getopt::Long;
my $filename;
my $n_threads = 1;
my $m_epochs  = 32;
my $help      = 0;

GetOptions (
	"filename=s"	=> \$filename,
	"n-threads=i"	=> \$n_threads,
	"m-epochs=i"	=> \$m_epochs,
	"help!"         => \$help,
);

if ($help){
	say "\n\tUSAGE:";
        say "\t\tperl sample.pl --filename=s --n[-threads]=i --m[-epochs]=i";
	say "\t\tperl sample.pl --help\n";
	exit(0);
}


# FORGING GRAYSCALE FILENAME ----------------------------------------------------
my $new_filename = $filename =~ s/\.png/-gray.png/ri;

# RUNNING THE TESTER ------------------------------------------------------------
my @samples;

for my $t (1..$n_threads){
	$samples[$t] = [];
	
	say "Corriendo para número de hilos [-n] = $t\n";
	for my $e (1..$m_epochs){
		say "\tCorriendo ciclo $e...";
		say "-" x 80;

		my $ellapsed_time = `./bin/procesador $filename $new_filename -n $t`;
		chomp($ellapsed_time);

		push @{$samples[$t]}, $ellapsed_time;
		say "-" x 80;

		say "\t Tiempo fue: $ellapsed_time.\n";
	}
}


# PRINTING AVERAGES TO STDOUT  --------------------------------------------------

use List::Util qw( sum );

say "\nRESULTADOS:";
say "-" x 80;
for my $t (1..$n_threads){
	my $running_sum = sum @{$samples[$t]};
	my $avg         = $running_sum / $m_epochs;

	printf "%-2d\t%-2.4f\n", $t, $avg;
}

say "\n";

